import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterListPage } from './register-list';

@NgModule({
  declarations: [
    RegisterListPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterListPage),
  ],
})
export class RegisterListPageModule {}
