import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterGamePage } from './register-game';

@NgModule({
  declarations: [
    RegisterGamePage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterGamePage),
  ],
})
export class RegisterGamePageModule {}
